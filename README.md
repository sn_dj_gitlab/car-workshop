# Car Workshop

## Commands

```sh
$ npm install
$ npm run dev
$ npm run test:unit
$ npm run test:e2e:dev
$ npm run lint
```


## Wheel Story

As a User, I want a Wheel that I can add to my car.  My car should have 4 wheels.  If the tire pressure is greater than 25, it should be good.   If the tire pressure is less than 25 it's bad.

What unit tests can we write for the wheel?
